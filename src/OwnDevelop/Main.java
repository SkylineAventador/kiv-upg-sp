package OwnDevelop;

import TrafficSim.Simulator;

import javax.swing.*;
import java.awt.*;

public class Main {
    public static Timer mainTimer;
    private static int inputScenarioIndex;
    public static void main(String[] args) {
        Simulator simulator = new Simulator();


        JFrame frame = new JFrame();
        // Vlastni graficky obsah
        DrawingPanel panel = new DrawingPanel(simulator);
        panel.setPreferredSize(new Dimension(1280, 720));


        if (args.length != 0 && Character.isDigit(args[0].charAt(0))) {
            inputScenarioIndex = Integer.parseInt(args[0].substring(0, 1));
            panel.setAndRunScenario(inputScenarioIndex); // Nastaveni scenare simulatoru a jeho spusteni.
            System.out.println("Running scenario #" + args[0].substring(0, 1) + ", \"" + simulator.getScenarios()[inputScenarioIndex] + "\"");
        } else {
            inputScenarioIndex = 0;
            panel.setAndRunScenario(0); //Default
            System.err.println("Error: Scenario input index missing or invalid value format. Running default scenario.");
        }
        if (panel.loadSavedScenarioMap(simulator.getScenarios()[inputScenarioIndex])){
            mainTimer = new Timer(50, e -> {
                simulator.nextStep(0.05);
                panel.repaint();
            });
            mainTimer.start();
        } else {
            mainTimer = new Timer(15, e -> {
                simulator.nextStep(0.25);
                panel.repaint();
            });
            mainTimer.start();
        }

        frame.add(panel);

        // Standardni manipulace s oknem
        frame.setTitle("TrafficSim - Initial");
        frame.pack();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }

    public static void initiateSimulation(Simulator simulator, DrawingPanel panel) {
        mainTimer.stop();
        simulator.clear(); // May we'll be needed to restart
        panel.loadSavedScenarioMap(simulator.getScenarios()[inputScenarioIndex]);
        simulator.runScenario(simulator.getScenarios()[inputScenarioIndex]);
        mainTimer = new Timer(50, e -> {
            simulator.nextStep(0.05);
            panel.repaint();
        });
        mainTimer.start();
    }
}
