package OwnDevelop;

public class SavedCarsPathCoords {
    private final double X;
    private final double Y;
    private final double orientation;

    public SavedCarsPathCoords(double x, double y, double orientation) {
        X = x;
        Y = y;
        this.orientation = orientation;
    }

    public double getX() {
        return X;
    }

    public double getY() {
        return Y;
    }

    public double getOrientation() {
        return orientation;
    }
}
