package OwnDevelop;

import TrafficSim.*;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

public class Servant {
    private final int SIZE_MULTIPLIER = 2;
    private final int PERSONAL_CAR_LENGTH = 5 * SIZE_MULTIPLIER;
    private final int MEDIUM_TRUCK_LENGTH = 7 * SIZE_MULTIPLIER; //Big trucks are yellow by default
    private double md_RealCoords_X[] = new double[2];
    private double md_RealCoords_Y[] = new double[2];

    private final int BOX_MARGIN = 10;
    private int minX_Point, minY_Point, maxX_Point, maxY_Point;

    private ArrayList<SavedCarsPathCoords> carsPathCoords = new ArrayList<>(20005);
    private final int CARS_DATA_COLLECTION_LIMIT = 20000;
    private int carLength;
    private int carHeight;

    public void drawTrafficState(Simulator sim, Graphics2D g) {
        g.setColor(Color.BLACK);

        carLength = (int) sim.getCars()[0].getLength() * SIZE_MULTIPLIER;
        carHeight = (int) sim.getRoadSegments()[0].getLaneWidth() * SIZE_MULTIPLIER;

        drawCollectedData(g);

        g.setColor(Color.YELLOW);
        for (Car car : sim.getCars()) {
            drawCar(model2window(car.getPosition()), car.getOrientation(), (int) car.getLength() * SIZE_MULTIPLIER, carHeight, g);
        }
    }

    public void collectTrafficStateData(Simulator sim, Graphics2D g, int waitTimeLeft) {
        if (carsPathCoords.size() <= CARS_DATA_COLLECTION_LIMIT) {
            g.scale(1, -1);

            g.setColor(Color.BLACK);
            g.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, 30));
            g.drawString("Processing scenario, please wait " + waitTimeLeft + " sec.", 100, -100);

            g.scale(1, -1);
            for (Car car : sim.getCars()) {
                collectCarsData(model2window(car.getPosition()), car.getOrientation());
            }
        }
    }

    public void drawCollectedData(Graphics2D g) {
        g.setColor(Color.BLACK);

        for (SavedCarsPathCoords pathPiece : carsPathCoords) {
            AffineTransform initTransform = g.getTransform();
            g.translate(pathPiece.getX(), pathPiece.getY());
            g.rotate(pathPiece.getOrientation());

            g.fillRect(0 - carLength, 0 - (carHeight / 2), carLength, carHeight);
            g.setTransform(initTransform);
        }
    }

    public void drawCrossRoad(CrossRoad cr, Graphics2D g) {
        //crossRoadsLanes = cr.getLanes();
        for (RoadSegment road : cr.getRoads()) {
            if (road!=null){
                drawRoadSegment(road, g);
            }
        }
//        for (int i = 0; i < crossRoadsLanes.length; i++) {
//            for (int j = i; j < crossRoadsLanes.length; j++) {
//                drawLane(model2window(crossRoadsLanes[i].getStartRoad().getStartPosition()),
//                        model2window(crossRoadsLanes[j].getStartRoad().getEndPosition()),
//                        (int) cr.getRoads()[0].getLaneWidth(), g);
//            }
//        }
    }

    private Point2D[] adjustLaneCoords(RoadSegment roadSegment, int count) {
        Point2D[] result = new Point2D[2];

//        double x1, x2, y1, y2, vx, vy, vLength, vNormX, vNormY, vArrowX, vArrowY, kx, ky;
//        x1 = roadSegment.getStartPosition().getX();
//        x2 = roadSegment.getEndPosition().getX();
//
//        y1 = roadSegment.getStartPosition().getY();
//        y2 = roadSegment.getEndPosition().getY();
//
//        vx = x2 - x1;
//        vy = y2 - y1;
//
//        vLength = Math.sqrt(vx * vx + vy * vy);
//
//        vNormX = vx / vLength;
//        vNormY = vy / vLength;
//
//        vArrowX = vNormX * vx;
//        vArrowY = vNormY * vy;
//
//        kx = -vArrowY;
//        ky = vArrowX;
//
//        kx *= 0.050;
//        ky *= 0.050;
//
//        result[0] = new Point2D.Double(x1, (y1 - ky)); //Start coords
//        result[1] = new Point2D.Double(x2, (y2 - ky)); //End coords
        double x1, x2, y1, y2, x1p, x2p, y1p, y2p, vx, vy, offset, L;
        offset = count * roadSegment.getLaneWidth();



        x1 = roadSegment.getStartPosition().getX();
        y1 = roadSegment.getStartPosition().getY();

        x2 = roadSegment.getEndPosition().getX();
        y2 = roadSegment.getEndPosition().getY();

        //Slozky smeroveho vektoru
        vx = x1 - x2;
        vy = y2 - y1;

        //Source: https://stackoverflow.com/questions/2825412/draw-a-parallel-line
        L = Math.hypot(vx, vy);

        x1p = x1 + ((offset * vy) / L);
        x2p = x2 + ((offset * vy) / L);
        y1p = y1 + ((offset * vx) / L);
        y2p = y2 + ((offset * vx) / L);

        //=====================================================================

        if (count > 0) {
            if (x1p == x2p) {
                x1p -= roadSegment.getLaneWidth();
                x2p -= roadSegment.getLaneWidth();
            }
        }
        y1p += roadSegment.getLaneWidth();
        y2p += roadSegment.getLaneWidth();
        result[0] = new Point2D.Double(x1p, y1p);
        result[1] = new Point2D.Double(x2p, y2p);

        return result;
    }

    public void drawRoadSegment(RoadSegment road, Graphics2D g) {
        Point2D[] adjustedLaneCords;
        Color color = g.getColor();

        g.setColor(Color.RED);
        for (int i = -1; i >= road.getBackwardLanesCount() * -1; i--) {
            adjustedLaneCords = adjustLaneCoords(road, i);
            drawLane(model2window(adjustedLaneCords[0]), model2window(adjustedLaneCords[1]), (int) road.getLaneWidth(), g);
        }

        g.setColor(Color.GREEN);
        for (int i = 1; i <= road.getForwardLanesCount(); i++) {
            adjustedLaneCords = adjustLaneCoords(road, i);
            drawLane(model2window(adjustedLaneCords[0]), model2window(adjustedLaneCords[1]), (int) road.getLaneWidth(), g);
        }
        g.setColor(color);
    }

    public void drawLane(Point2D start, Point2D end, int size, Graphics2D g) {
        BasicStroke stroke = (BasicStroke)g.getStroke();
        g.setStroke(new BasicStroke(size*SIZE_MULTIPLIER));
        g.drawLine((int) start.getX(), (int) start.getY(), (int) end.getX(), (int) end.getY());
        g.setStroke(stroke);
    }


    public void drawCar(Point2D position, double orientation, int carLength, int carHeight, Graphics2D g) {
        AffineTransform initTransform = g.getTransform();
        g.translate(position.getX(), position.getY());
        g.rotate(orientation);

        if (carLength < PERSONAL_CAR_LENGTH) {
            g.setColor(Color.MAGENTA);
        } else if (carLength < MEDIUM_TRUCK_LENGTH) {
            g.setColor(Color.CYAN);
        }
        g.fillRect(0 - carLength, 0 - (carHeight / 2), carLength, carHeight);
        g.setColor(Color.YELLOW);

        g.setTransform(initTransform);
    }

    /**
     * TODO Change this later
     */
    public void computeModelDimensions(Simulator simulator) {
        /*
        Rovnice prevodu
        (d-minR)/(maxR-minR)*(maxO-minO)+minO;

        1m = 1000mm
        1'' = 25.4mm
        */
        RoadSegment[] roadSegments = simulator.getRoadSegments();
        ArrayList<Double> tempXArray = new ArrayList<Double>(roadSegments.length),
                tempYArray = new ArrayList<Double>(roadSegments.length);

        for (RoadSegment roadSegment : roadSegments) {
            tempXArray.add(roadSegment.getStartPosition().getX());
            tempXArray.add(roadSegment.getEndPosition().getX());

            tempYArray.add(roadSegment.getStartPosition().getY());
            tempYArray.add(roadSegment.getEndPosition().getY());
        }
        Collections.sort(tempXArray);
        Collections.sort(tempYArray);

        md_RealCoords_X[0] = tempXArray.get(0);
        md_RealCoords_X[1] = tempXArray.get(tempXArray.size() - 1);

        md_RealCoords_Y[0] = tempYArray.get(0);
        md_RealCoords_Y[1] = tempYArray.get(tempYArray.size() - 1);
    }

    /**
     * TODO Change this later
     * @param width
     * @param height
     */
    public void computeModel2WindowTransformation(int width, int height) {
        int r = Math.min(width, height);
        minX_Point = BOX_MARGIN;
        maxX_Point = r - BOX_MARGIN;

        minY_Point = BOX_MARGIN;
        maxY_Point = r - BOX_MARGIN;
    }

    /**
     * todo Change this later.
     * @return Correct coordinates
     */
    private Point2D model2window(Point2D p) {
        double x;
        double y;
        double scale = (maxX_Point - minX_Point) / (md_RealCoords_X[1] - md_RealCoords_X[0]);

        x = (p.getX() - minX_Point) * scale;
        y = (p.getY() - md_RealCoords_Y[0]) / (md_RealCoords_Y[1] - md_RealCoords_Y[0]) * (maxY_Point - minY_Point) + minY_Point;

        return new Point2D.Double(x, y);
    }

    private void collectCarsData(Point2D position, double orientation) {
        if (!Double.isNaN(position.getX()) && !Double.isNaN(position.getY()) &&
                !Double.isNaN(orientation) &&
                position.getX() > 0 && position.getY() > 0) {
            carsPathCoords.add(new SavedCarsPathCoords(position.getX(), position.getY(), orientation));
        }
    }

    public int getCarsPathCoordsSize() {
        return carsPathCoords.size();
    }

    public int getCarsDataCollectionLimit() {
        return CARS_DATA_COLLECTION_LIMIT;
    }
}
