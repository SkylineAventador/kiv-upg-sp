package OwnDevelop;

import TrafficSim.*;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

public class DrawingPanel extends JPanel {
    private static final long serialVersionUID = 1L;
    private Simulator simulator;
    private String runningScenarioName;
    private Servant servant;
    private long startTime, startElapsedTime;
    private BufferedImage resultScenarioMap;
    private boolean isDataCollecting = true;


    public DrawingPanel(Simulator simulator) {
        this.simulator = simulator;
        servant = new Servant();
        startTime = System.currentTimeMillis();
        resultScenarioMap = new BufferedImage(1280, 720, BufferedImage.TYPE_INT_RGB);
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        Graphics2D g2d = (Graphics2D) g;
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2d.setRenderingHint(RenderingHints.KEY_STROKE_CONTROL, RenderingHints.VALUE_STROKE_PURE);
        g2d.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_SPEED);


        g2d.translate(0, this.getHeight());
        g2d.scale(1, -1);
        servant.computeModelDimensions(simulator);
        servant.computeModel2WindowTransformation(this.getWidth(), this.getHeight());
        startElapsedTime = System.currentTimeMillis() - startTime;

        if (isDataCollecting) {
            if (startElapsedTime < 15000) {
                servant.collectTrafficStateData(simulator, g2d, (int) (15000 - startElapsedTime) / 1000);
            }

            if (startElapsedTime > 15000) {
                saveCurrentScenarioMap(runningScenarioName, g2d);
                Main.initiateSimulation(simulator, this);
            }
        } else {
            g2d.drawImage(resultScenarioMap, 0, 0, null);
            servant.drawTrafficState(simulator, g2d);
        }
    }

    public Simulator getSimulator() {
        return simulator;
    }

    public void setAndRunScenario(int index) {
        try {
            runningScenarioName = simulator.getScenarios()[index];
            simulator.runScenario(simulator.getScenarios()[index]);
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("Chyba: Pozadovany scenar nebyl nalezen.");
            e.printStackTrace();
            System.exit(1);
        }
    }

    private void drawContentToImage(BufferedImage image) {
        Graphics2D g2 = image.createGraphics();

        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2.setRenderingHint(RenderingHints.KEY_STROKE_CONTROL, RenderingHints.VALUE_STROKE_PURE);
        g2.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_SPEED);


        g2.translate(0, image.getHeight());
        g2.scale(1, -1);
        g2.setColor(Color.WHITE);
        g2.fillRect(image.getMinX(), image.getMinY(), image.getWidth(), image.getHeight());
        servant.computeModelDimensions(simulator);
        servant.computeModel2WindowTransformation(image.getWidth(), image.getHeight());
        servant.drawCollectedData(g2);
    }

    private void saveCurrentScenarioMap(String scenarioName, Graphics2D g2) {
        drawContentToImage(resultScenarioMap);
        System.out.println("Mapa scenaru \"" + scenarioName + "\" byla uspesne ulozena");
        try {
            ImageIO.write(resultScenarioMap, "png", new File("./ScenarioMaps/" + scenarioName + ".png"));
        } catch (IOException e) {
            System.out.println("Nepodarilo se zapsat obrazek `" + scenarioName + "'.");
        }
    }

    public boolean loadSavedScenarioMap(String scenarioName) {
        try {
            resultScenarioMap = ImageIO.read(new File("./ScenarioMaps/" + scenarioName + ".png"));
            isDataCollecting = false;
            return true;
        }
        catch (IOException e) {
            isDataCollecting = true;
            return false;
        }
    }
}
